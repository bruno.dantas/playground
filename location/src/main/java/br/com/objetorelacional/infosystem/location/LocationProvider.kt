package br.com.objetorelacional.infosystem.location

import android.location.Location
import com.google.android.gms.location.LocationRequest
import com.patloew.rxlocation.RxLocation
import io.reactivex.Observable
import io.reactivex.Single
import java.util.concurrent.TimeUnit

interface LocationProvider {
    fun getLocation(): Single<Location>
    fun getLocations(): Observable<Location>
    fun getLocations(locationRequest: LocationRequest): Observable<Location>
}

class AndroidLocationProvider(private val rxLocation: RxLocation): LocationProvider {

    private fun getDefaultRequestOnce(): LocationRequest =
        LocationRequest()
            .apply {
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                interval = 100
                fastestInterval = 100
                numUpdates = 1
                setExpirationDuration(5*1000)
            }

    private fun getDefaultRequest(): LocationRequest =
        LocationRequest()
            .apply {
                priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
                interval = 20 * 1000
                fastestInterval = 10 * 1000
                maxWaitTime = 20 * 1000
            }

    override fun getLocation(): Single<Location> {
        val locationRequest = getDefaultRequestOnce()
        return rxLocation.location()
            .updates(locationRequest)
            .timeout(5, TimeUnit.SECONDS)
            .firstOrError()
    }

    override fun getLocations(locationRequest: LocationRequest): Observable<Location> {
        return rxLocation.location()
            .updates(locationRequest)
    }

    override fun getLocations(): Observable<Location> {
        val locationRequest = getDefaultRequest()
        return rxLocation.location()
            .updates(locationRequest)
    }
}
