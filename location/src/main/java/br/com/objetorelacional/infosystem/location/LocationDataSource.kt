package br.com.objetorelacional.infosystem.location

import android.location.Location
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.ReplaySubject

class LocationDataSource {
    private val locations = BehaviorSubject.create<Location>()
    val locationsReplay = ReplaySubject.create<Location>()

    fun getLocations(): Observable<Location> = locationsReplay

    fun addLocation(location: Location) {
        locationsReplay.value?.let {  oldLocation ->
            if (location.distanceTo(oldLocation) >= 50f ||
                    (location.time - oldLocation.time) >= 30 * 1000) {
                locationsReplay.onNext(location)
            }

        } ?: locationsReplay.onNext(location)
    }
}