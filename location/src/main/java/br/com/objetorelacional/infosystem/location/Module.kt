package br.com.objetorelacional.infosystem.location

import com.patloew.rxlocation.RxLocation
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val locationModule = module {
    single { RxLocation(androidContext()) }
    single { LocationDataSource() }
    factory<LocationProvider> { AndroidLocationProvider(get()) }
}