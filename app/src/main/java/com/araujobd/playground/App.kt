package com.araujobd.playground

import android.app.Application
import br.com.objetorelacional.infosystem.location.locationModule
import com.araujobd.playground.form.formModule
import com.araujobd.playground.location.locationsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        val appModules = listOf(formModule, locationsModule, locationModule)

        startKoin {
            androidContext(this@App)
            modules(appModules)
        }
    }

}
