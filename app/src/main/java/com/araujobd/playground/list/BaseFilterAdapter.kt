package com.araujobd.playground.list

abstract class BaseFilterAdapter<T>(items: List<T> = emptyList()) : BaseAdapter<T>(items) {

    private var originalItems: List<T> = items

    abstract fun filterItem(model: T, value: String): Boolean

    override fun update(items: List<T>) {
        this.originalItems = items
        super.update(items)
    }

    fun filter(value: String) {
        val filtered = originalItems.filter { filterItem(it, value) }
        super.update(filtered)
    }

}