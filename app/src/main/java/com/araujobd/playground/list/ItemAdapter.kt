package com.araujobd.playground.list

import com.araujobd.playground.R

class ItemAdapter(models: List<ItemModel> = emptyList()): BaseFilterAdapter<ItemModel>(models) {

    override val layoutId = R.layout.item_list

    override fun areItemsTheSame(oldItem: ItemModel, newItem: ItemModel) = oldItem == newItem

    override fun filterItem(model: ItemModel, value: String): Boolean =
        model.name.contains(value, true) || model.unit.contains(value, true)
}
