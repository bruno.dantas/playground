package com.araujobd.playground.list

data class ItemModel(val name: String, val unit: String)

fun getItems() = listOf(
    ItemModel("Item 1", "UND1"),
    ItemModel("Item 2", "UND2"),
    ItemModel("Item 3", "UND1"),
    ItemModel("Item 4", "UND2"),
    ItemModel("Item 5", "UND1"),
    ItemModel("Item 6", "UND3"),
    ItemModel("Item 7", "UND4")
)