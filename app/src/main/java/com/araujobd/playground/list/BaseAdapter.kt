package com.araujobd.playground.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.araujobd.playground.R

abstract class BaseAdapter<T>(protected var items: List<T> = emptyList())
    : RecyclerView.Adapter<BaseAdapter.ViewHolder>() {

    protected abstract val layoutId: Int
    abstract fun areItemsTheSame(oldItem: T, newItem: T): Boolean

    open fun update(items: List<T>) {
        val diffResult = calculateDiff(items)
        this.items = items

        diffResult.dispatchUpdatesTo(this)
    }

    open fun calculateDiff(items: List<T>): DiffUtil.DiffResult {
        val diffCallBack = DiffCallBack(
            this.items,
            items,
            ::areItemsTheSame
        )

        return DiffUtil.calculateDiff(diffCallBack)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context), layoutId, parent, false
        )
    )

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.binding.setVariable(BR.item, item)
        holder.binding.executePendingBindings()
    }

        class ViewHolder(val binding: ViewDataBinding): RecyclerView.ViewHolder(binding.root)
}

