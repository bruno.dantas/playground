package com.araujobd.playground.list

import androidx.recyclerview.widget.LinearLayoutManager
import com.araujobd.playground.R
import com.araujobd.playground.base.BaseFragment
import com.araujobd.playground.databinding.FragmentListBinding
import com.jakewharton.rxbinding2.widget.RxTextView

class ListFragment : BaseFragment<FragmentListBinding>(R.layout.fragment_list) {

    private val items by lazy { getItems() }
    private val itemsAdapter by lazy { ItemAdapter(items) }

    override fun onViewCreated() {
        binding.rvList.apply {
            layoutManager = LinearLayoutManager(requireContext())
            adapter = itemsAdapter
        }

        RxTextView.textChanges(binding.etFilter)
            .skipInitialValue()
            .map { it.toString() }
            .doOnNext { itemsAdapter.filter(it) }
            .subscribe({}, {})
            .track()
        }
}
