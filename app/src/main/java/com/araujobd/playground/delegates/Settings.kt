package com.araujobd.playground.delegates

import android.content.SharedPreferences
import androidx.core.content.edit
import io.reactivex.Single
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

data class Token(val token: String,val userIs: String)

class Settings(private val preferences: SharedPreferences) {
    var total by preferences.int()
    var userId by preferences.stringNullabe()
    var token by preferences.stringNullabe()
    var name: String? by preferences.stringNullabe()

    fun saveToken(token: Token) {
        this.token = token.token
        userId = token.userIs
    }

    fun unsetToken() {
        token = null
        userId = null
    }

}

/*
fun SharedPreferences.int(default: Int  = 0, key: String? = null): ReadWriteProperty<Any, Int> {
    return object : ReadWriteProperty<Any, Int> {
        override fun getValue(thisRef: Any, property: KProperty<*>) =
            getInt(key ?: property.name, default)

        override fun setValue(thisRef: Any, property: KProperty<*>, value: Int) =
            edit { putInt(key ?: property.name, value) }
    }
}
*/

private inline fun <T> SharedPreferences.delegate(
    defaultValue: T,
    key: String?,
    crossinline getter: SharedPreferences.(String, T) -> T,
    crossinline setter: SharedPreferences.Editor.(String, T) -> SharedPreferences.Editor
): ReadWriteProperty<Any, T> {
    return object : ReadWriteProperty<Any, T> {
        override fun getValue(thisRef: Any, property: KProperty<*>) =
            getter(key ?: property.name, defaultValue)

        override fun setValue(thisRef: Any, property: KProperty<*>,
                              value: T) =
            edit { setter(key ?: property.name, value) }
    }
}

fun SharedPreferences.string( default: String = "", key: String? = null ) =
    delegate(default, key, SharedPreferences::getString, SharedPreferences.Editor::putString)

fun SharedPreferences.stringNullabe(default: String? = null, key: String? = null ) =
    delegate(default, key, SharedPreferences::getString, SharedPreferences.Editor::putString)


fun SharedPreferences.int(defaultValue: Int = 0, key: String? = null) =
    delegate(defaultValue, key, SharedPreferences::getInt, SharedPreferences.Editor::putInt)

fun String?.single(): Single<String> =
    Single.create { emitter ->
        this?.let { emitter.onSuccess(it) }
            ?: emitter.onError(IllegalStateException(""))
    }
