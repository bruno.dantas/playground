package com.araujobd.playground.baseform

import android.view.View
import com.araujobd.playground.R
import com.araujobd.playground.base.BaseFragment
import com.araujobd.playground.customform.view.Step
import com.araujobd.playground.databinding.FragmentBaseFormBinding
import kotlinx.android.synthetic.main.appbar.view.*

abstract class BaseFormFragment: BaseFragment<FragmentBaseFormBinding>(R.layout.fragment_base_form) {

    abstract val step: Step

    override fun onViewCreated() {
        binding.steps = step
        binding.includeAppbar.toolbar.title = "Register Form"
    }

    fun setForm(form: View) {
        binding.frameContainer.addView(form)
    }
}