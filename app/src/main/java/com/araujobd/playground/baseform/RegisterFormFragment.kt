package com.araujobd.playground.baseform

import com.araujobd.playground.customform.view.Step
import com.araujobd.playground.databinding.FragmentRegisterFormBinding

class RegisterFormFragment : BaseFormFragment() {

    private lateinit var formBinding: FragmentRegisterFormBinding
    override val step = Step("Name", "bottom")

    override fun onViewCreated() {
        super.onViewCreated()
        formBinding = FragmentRegisterFormBinding.inflate(layoutInflater)
        setForm(formBinding.root)
    }
}