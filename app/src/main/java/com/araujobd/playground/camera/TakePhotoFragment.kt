package com.araujobd.playground.camera

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.hardware.display.DisplayManager
import android.os.Bundle
import android.os.Environment
import android.util.DisplayMetrics
import android.util.Log
import android.util.Rational
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.camera.core.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.araujobd.playground.camera.utils.AutoFitPreviewBuilder
import com.araujobd.playground.databinding.FragmentTakePhotoBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_take_photo.*
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

val permissions = arrayOf(
    Manifest.permission.CAMERA,
    Manifest.permission.WRITE_EXTERNAL_STORAGE,
    Manifest.permission.READ_EXTERNAL_STORAGE)

class TakePhotoFragment : Fragment() {

    private lateinit var binding: FragmentTakePhotoBinding

    private lateinit var outputDirectory: File

    private var displayId = -1
    private var lensFacing = CameraX.LensFacing.BACK
    private var preview: Preview? = null
    private var imageCapture: ImageCapture? = null

    private lateinit var displayManager: DisplayManager

    private val imageSavedListener = object:  ImageCapture.OnImageSavedListener{
        override fun onImageSaved(file: File) {
            Snackbar.make(fab_take_photo, "Photo capture succeeded: ${file.absolutePath}", Snackbar.LENGTH_SHORT)
                .show()
            Log.v("IMAGE", "Successfully saved image")
        }

        override fun onError(
            useCaseError: ImageCapture.UseCaseError,
            message: String,
            cause: Throwable?
        ) {
            Snackbar.make(fab_take_photo, "Photo capture failed: $message", Snackbar.LENGTH_SHORT)
                .show()
            Log.e("IMAGE", message)
        }
    }

    private val displayListener = object : DisplayManager.DisplayListener {
        override fun onDisplayAdded(displayId: Int) = Unit
        override fun onDisplayRemoved(displayId: Int) = Unit
        override fun onDisplayChanged(displayId: Int) = view?.let { view ->
            if (displayId == this@TakePhotoFragment.displayId) {
                Log.d("CAMERA", "Rotation changed: ${view.display.rotation}")
                preview?.setTargetRotation(view.display.rotation)
                imageCapture?.setTargetRotation(view.display.rotation)
            }
        } ?: Unit
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentTakePhotoBinding.inflate(inflater, container, false)
            .run {
                binding = this
                lifecycleOwner = this@TakePhotoFragment.viewLifecycleOwner
                root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        outputDirectory = getOutputDirectory(requireContext())

        displayManager = binding.viewFinder.context
            .getSystemService(Context.DISPLAY_SERVICE) as DisplayManager
        displayManager.registerDisplayListener(displayListener, null)

        binding.viewFinder.post {
            displayId = binding.viewFinder.display.displayId

            bindCamera()
        }

        binding.fabTakePhoto.setOnClickListener { takePhoto() }
    }

    override fun onResume() {
        super.onResume()

        if (hasNoPermissions()) {
            requestPermission()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()

        displayManager.unregisterDisplayListener(displayListener)
    }

    private fun bindCamera() {

        val screenAspectRatio = Rational(4, 3)
        val targetResolution = Size(640, 380)

        val viewFinderConfig = PreviewConfig.Builder().apply {
            setLensFacing(lensFacing)
//            setTargetResolution(targetResolution)
            // We request aspect ratio but no resolution to let CameraX optimize our use cases
//            setTargetAspectRatio(screenAspectRatio)
            // Set initial target rotation, we will have to call this again if rotation changes
            // during the lifecycle of this use case
            setTargetRotation(binding.viewFinder.display.rotation)
        }.build()

        // Use the auto-fit preview builder to automatically handle size and orientation changes
        preview = AutoFitPreviewBuilder.build(viewFinderConfig, binding.viewFinder)

        val imageCaptureConfig = ImageCaptureConfig.Builder().apply {
            setLensFacing(lensFacing)
            setCaptureMode(ImageCapture.CaptureMode.MAX_QUALITY)
            setTargetAspectRatio(screenAspectRatio)
//            setTargetResolution(targetResolution)
            setTargetRotation(binding.viewFinder.display.rotation)
        }.build()

        imageCapture = ImageCapture(imageCaptureConfig)

        CameraX.bindToLifecycle(viewLifecycleOwner, imageCapture, preview)
    }

    private fun hasNoPermissions(): Boolean{
        return ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(requireContext(),
            Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
    }

    /**
     * Request all permissions
     */
    fun requestPermission(){
        ActivityCompat.requestPermissions(requireActivity(), permissions,0)
    }

    fun takePhoto() {
        val outputFile = createFile(outputDirectory, "yyyy-MM-dd-HH-mm-ss-SSS", ".jpg")
        imageCapture?.takePicture(outputFile, imageSavedListener)
    }

    fun toggleFlash() {
        val flashMode = imageCapture?.flashMode
        if(flashMode == FlashMode.ON) imageCapture?.flashMode = FlashMode.OFF
        else imageCapture?.flashMode = FlashMode.ON
    }

    private fun createFile(baseFolder: File, format: String, extension: String) =
        File(baseFolder, SimpleDateFormat(format, Locale.US)
            .format(System.currentTimeMillis()) + extension)

    fun getOutputDirectory(context: Context): File {
        val appContext = context.applicationContext
        val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
            File(it, "playground").apply { mkdirs() } }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else appContext.filesDir
    }
}