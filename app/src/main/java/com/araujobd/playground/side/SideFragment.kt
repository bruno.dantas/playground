package com.araujobd.playground.side

import android.content.Context
import android.util.DisplayMetrics
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import com.araujobd.playground.R
import com.araujobd.playground.base.BaseFragment
import com.araujobd.playground.databinding.FragmentSideBinding


class SideFragment : BaseFragment<FragmentSideBinding>(R.layout.fragment_side) {

    override fun onViewCreated() {
        setfullwidth()
        val toggle = ActionBarDrawerToggle(requireActivity(), binding.drawer, R.string.nav_app_bar_open_drawer_description,
            R.string.nav_app_bar_open_drawer_description)
        binding.drawer.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun setfullwidth() {
        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay.getMetrics(displayMetrics)
        val params =
            binding.navView.layoutParams as DrawerLayout.LayoutParams
        params.width = displayMetrics.widthPixels - requireContext().convertDpToPx(16f).toInt()
        binding.navView.layoutParams = params
    }
}

fun Context.convertDpToPx(dp: Float): Float {
    return dp * resources.displayMetrics.density
}