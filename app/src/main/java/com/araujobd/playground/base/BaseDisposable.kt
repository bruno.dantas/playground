package com.araujobd.playground.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

interface BaseDisposable {
    fun addDisposable(disposable: Disposable)
    fun dispose()
    fun Disposable.track()
}

open class BaseDisposableImpl : BaseDisposable {

    private val compositeDisposable by lazy { CompositeDisposable() }

    override fun addDisposable(disposable: Disposable) {
        compositeDisposable.add(disposable)
    }

    override fun dispose() {
        if (!compositeDisposable.isDisposed)
            compositeDisposable.dispose()
    }

    override fun Disposable.track() {
        addDisposable(this)
    }
}
