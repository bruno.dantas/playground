package com.araujobd.playground.base

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import java.text.SimpleDateFormat
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.*

fun <T : Any, L : LiveData<T>> LifecycleOwner.observe(liveData: L, body: (T) -> Unit) {
    liveData.observe(this, Observer(body))
}

fun Date.showDate(): String {
    return SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        .format(this)
}

fun Date.showTime(): String {
    return SimpleDateFormat("HH:mm", Locale.getDefault())
        .format(this)
}

fun Date.showDateTime(): String {
    return SimpleDateFormat("dd/MM/yyyy HH:mm:ss", Locale.getDefault())
        .format(this)
}
