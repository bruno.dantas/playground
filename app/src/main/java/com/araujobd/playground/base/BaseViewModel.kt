package com.araujobd.playground.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.Disposable

abstract class BaseViewModel : ViewModel(), BaseDisposable by BaseDisposableImpl() {

    private val _message: MutableLiveData<SingleEvent<String>> = MutableLiveData()

    val message: LiveData<SingleEvent<String>>
        get() = _message

    protected fun showMessage(message: String) {
        _message.postValue(SingleEvent(message))
    }

    override fun onCleared() {
        dispose()
        super.onCleared()
    }
}
