package com.araujobd.playground.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.appbar.MaterialToolbar
import io.reactivex.Observable

abstract class BaseFragment<TBinding : ViewDataBinding>(
    @LayoutRes private val layout: Int
) : Fragment(), BaseDisposable by BaseDisposableImpl() {

    protected lateinit var binding: TBinding

    open fun onViewCreated() {}

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = DataBindingUtil
        .inflate<TBinding>(inflater, layout, container, false)
        .run {
            binding = this
            lifecycleOwner = viewLifecycleOwner
            root
        }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        onViewCreated()
    }

    override fun onDestroy() {
        dispose()
        super.onDestroy()
    }

    protected fun setupToolbar(toolbar: MaterialToolbar, appBarConfiguration: AppBarConfiguration) {
        toolbar.setupWithNavController(findNavController(), appBarConfiguration)
    }
}
