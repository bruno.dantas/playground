package com.araujobd.playground.date

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.araujobd.playground.base.SingleEvent
import com.araujobd.playground.base.observe
import com.araujobd.playground.base.showDate
import com.araujobd.playground.base.showTime
import com.araujobd.playground.databinding.FragmentDateBinding
import java.util.*

class DateFragment : Fragment() {

    private lateinit var binding: FragmentDateBinding
    private val dateHandler by lazy{ DateHandler() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentDateBinding.inflate(inflater, container, false)
            .run {
                binding = this
                handler = dateHandler
                lifecycleOwner = this@DateFragment.viewLifecycleOwner
                root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        observe(dateHandler.chooseDate) {
            val calendar = Calendar.getInstance()
            DatePickerDialog(requireContext(),
                { _, year, month, day -> dateHandler.setDate(year, day, month) } ,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
                .show()
        }
        observe(dateHandler.chooseTime) {
            val calendar = Calendar.getInstance()
            TimePickerDialog(requireContext(),
                { _, hours, minutes -> dateHandler.setTime(hours, minutes) },
                calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE),
                true
            )
                .show()
        }
    }
}

class DateHandler {

    private val _chooseDate = MutableLiveData<SingleEvent<Boolean>>()
    val chooseDate: LiveData<SingleEvent<Boolean>> = _chooseDate

    private val _chooseTime = MutableLiveData<SingleEvent<Boolean>>()
    val chooseTime: LiveData<SingleEvent<Boolean>> = _chooseTime

    val fieldDate = MutableLiveData<Date>()
    val showDate: LiveData<String> = Transformations.map(fieldDate) { it.showDate() }

    val fiedTime = MutableLiveData<Date>()
    val showTime: LiveData<String> = Transformations.map(fiedTime) { it.showTime()}

    fun onChooseDate() {
        _chooseDate.postValue(SingleEvent(true))
    }

    fun onChooseTime() {
        _chooseTime.postValue(SingleEvent(true))
    }

    fun setDate(year: Int, dayOfMonth: Int, monthOfYear: Int) {
        Calendar.getInstance()
            .apply {
                set(year, monthOfYear, dayOfMonth)
                fieldDate.postValue(time)
            }
    }

    fun setTime(hours: Int, minutes: Int) {
        Calendar.getInstance()
            .apply {
                set(get(Calendar.YEAR), get(Calendar.MONTH), get(Calendar.DAY_OF_MONTH),
                    hours, minutes, 0)
                 this@DateHandler.fiedTime.postValue(time)
            }
    }
}
