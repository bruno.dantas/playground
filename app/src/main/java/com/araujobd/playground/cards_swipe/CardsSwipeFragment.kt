package com.araujobd.playground.cards_swipe

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import androidx.viewpager2.widget.ViewPager2.ORIENTATION_HORIZONTAL
import com.araujobd.playground.R
import kotlinx.android.synthetic.main.card_swipe_1.view.*
import kotlinx.android.synthetic.main.fragment_swipe.*

class CardsSwipeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.fragment_swipe, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViewPager2(ItemAdapter(listOf("1", "2", "3")))
        indicator.setViewPager(pager2)
        tv_content.text = "1"
        pager2.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                tv_content.text = (position+1).toString()
            }
        })

    }

    fun setupViewPager2(adapter: ItemAdapter) {
        pager2.adapter = adapter
        with(pager2) {
            clipToPadding = false
            clipChildren = false
            offscreenPageLimit = 1
        }
        val pageMarginPx = resources.getDimensionPixelOffset(R.dimen.pageMargin)
        val offsetPx = resources.getDimensionPixelOffset(R.dimen.offset)
        pager2.setPageTransformer { page, position ->
            val viewPager = page.parent.parent as ViewPager2
            val offset = position * -(2 * offsetPx + pageMarginPx)
            if (viewPager.orientation == ORIENTATION_HORIZONTAL) {
                if (ViewCompat.getLayoutDirection(viewPager) == ViewCompat.LAYOUT_DIRECTION_RTL) {
                    page.translationX = -offset
                } else {
                    page.translationX = offset
                }
            } else {
                page.translationY = offset
            }
        }

    }

}

class CardSwipeAdapter(private val items: List<Fragment>, fragment: Fragment): FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = items.size

    override fun createFragment(position: Int): Fragment {
        val fragment = items[position]
        fragment.arguments = Bundle().apply {
            putInt("position", position + 1)
        }
        return fragment
    }
}

class ItemAdapter(private var items: List<String> = listOf()): RecyclerView.Adapter<ItemAdapter.ItemAdapterViewHolder>() {

    class ItemAdapterViewHolder(view: View): RecyclerView.ViewHolder(view) {
        fun bind(text: String) {
            itemView.textView.text = text
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemAdapterViewHolder {
        return ItemAdapterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.card_swipe_1, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ItemAdapterViewHolder, position: Int) {
        holder.bind(items[position])
    }
}
