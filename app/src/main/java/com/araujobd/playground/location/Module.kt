package com.araujobd.playground.location

import com.patloew.rxlocation.RxLocation
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val locationsModule = module {

    viewModel { LocationViewModel(get()) }
}
