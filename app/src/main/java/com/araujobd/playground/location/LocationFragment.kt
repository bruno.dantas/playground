package com.araujobd.playground.location

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.araujobd.playground.R
import com.araujobd.playground.base.BaseFragment
import com.araujobd.playground.base.observe
import com.araujobd.playground.databinding.FragmentLocationBinding
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class LocationFragment : BaseFragment<FragmentLocationBinding>(R.layout.fragment_location) {

    private val locationViewModel by viewModel<LocationViewModel>()
    private val locationAdapter by lazy { LocationAdapter() }

    override fun onViewCreated() {
        checkPermissions()
        binding.viewmodel = locationViewModel
        binding.rvLocations.apply {
            adapter = locationAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        observe(locationViewModel.message) {
            it.getContentIfNotHandled()?.let {  error ->
                Snackbar.make(binding.root, error, Snackbar.LENGTH_LONG).show()
            }
        }

        observe(locationViewModel.onStart) { event ->
            event.getContentIfNotHandled()?.let {
                if (it) {
                    val intent = Intent(requireContext(), LocationUpdatesService::class.java)
                    ContextCompat.startForegroundService(requireContext(), intent)
                }
            }
        }

        observe(locationViewModel.onStop) { event ->
            event.getContentIfNotHandled()?.let {
                if (it) {
                    val intent = Intent(requireContext(), LocationUpdatesService::class.java)
                    requireContext().stopService(intent)
                }
            }
        }

        observe(locationViewModel.locations, locationAdapter::update)

    }
    private fun checkPermissions() {
        if (!hasPermissionGranted()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
            }
        }
    }

    private fun hasPermissionGranted(): Boolean {
        return ContextCompat.checkSelfPermission( requireContext(), Manifest.permission.ACCESS_FINE_LOCATION ) == PackageManager.PERMISSION_GRANTED
    }
}
