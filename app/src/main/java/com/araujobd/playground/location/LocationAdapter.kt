package com.araujobd.playground.location

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.araujobd.playground.R
import com.araujobd.playground.databinding.ItemLocationBinding

class LocationAdapter (private var items: List<LocationUiModel> = listOf()): RecyclerView.Adapter<LocationAdapter.ViewHolder>() {

    class ViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        val binding: ItemLocationBinding? = DataBindingUtil.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_location, parent, false))
    }

    fun update(items: List<LocationUiModel>) {
        this.items = items
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding?.apply {
            location = items[position]
            executePendingBindings()
        }

    }
}
