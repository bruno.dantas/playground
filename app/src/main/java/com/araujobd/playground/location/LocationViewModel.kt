package com.araujobd.playground.location

import android.location.Location
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import br.com.objetorelacional.infosystem.location.LocationDataSource
import com.araujobd.playground.base.BaseViewModel
import com.araujobd.playground.base.SingleEvent
import com.araujobd.playground.base.showDateTime
import java.util.*
import kotlin.math.roundToInt

class LocationViewModel (private val locationDataSource: LocationDataSource) : BaseViewModel() {

    val onStart = MutableLiveData<SingleEvent<Boolean>>()
    val onStop = MutableLiveData<SingleEvent<Boolean>>()

    private val _location = MutableLiveData<Location>()
    private val _locations = MutableLiveData<List<Location>>().apply { postValue(locationDataSource.locationsReplay.values.toList() as List<Location>)}
    val locations: LiveData<List<LocationUiModel>> = Transformations.map(_locations) {
        it.map { location ->
            with(location) {
                LocationUiModel(
                    provider,
                    latitude.toString(),
                    accuracy.toString(),
                    longitude.toString(),
                    Date(time).showDateTime(),
                    speed.times(3.6).roundToInt().toString() + "Km/h",
                    altitude.toString(),
                    hasAltitude()
                )

            }
        }
    }

    init {

        locationDataSource.getLocations()
            .doOnNext { location ->
                _locations.value?.let {  locations ->
                    _locations.postValue(locations.plus(location))
                }
            }
            .subscribe(_location::postValue) { error -> showMessage(error.localizedMessage) }
            .track()
    }

    fun onStartClicked() {
        onStart.postValue(SingleEvent(true))
    }

    fun onStopClicked() {
        onStop.postValue(SingleEvent(true))
    }
}

data class LocationUiModel(
    val provider: String,
    val latitude: String,
    val accuracy: String,
    val longitude: String,
    val lastUpdate: String,
    val speed: String,
    val elevation: String,
    val hasElevation: Boolean)