package com.araujobd.playground.location

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.widget.Toast
import androidx.core.app.NotificationCompat
import br.com.objetorelacional.infosystem.location.LocationDataSource
import br.com.objetorelacional.infosystem.location.LocationProvider
import br.com.objetorelacional.infosystem.location.R
import com.araujobd.playground.MainActivity
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import org.koin.core.KoinComponent
import org.koin.core.inject

class LocationUpdatesService: Service(), KoinComponent {

    private lateinit var executor: Scheduler
    private val CHANNEL_ID = "LocationUpdatesService"
    private val locationProvider : LocationProvider by inject()
    private val dataSource by inject<LocationDataSource>()
    private val disposables = CompositeDisposable()

    override fun onCreate() {
        super.onCreate()
        executor = Schedulers.newThread()
    }
    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        createNotificationChannel()
        val pendingIntent = Intent(Intent.ACTION_VIEW, Uri.parse("playground://location")).let {
            PendingIntent.getActivity(this, 0, it, 0)
        }
        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Atualizando Local")
            .setSmallIcon(R.drawable.common_google_signin_btn_icon_dark)
            .setContentIntent(pendingIntent)
            .build()
        startForeground(13, notification)

        getLocations()

        return START_NOT_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.dispose()
        executor.shutdown()
    }

    private fun getLocations(){
        disposables.add(
            locationProvider.getLocations()
            .subscribeOn(executor)
            .doOnNext { dataSource.addLocation(it) }
            .subscribe()
        )
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val serviceChannel = NotificationChannel(CHANNEL_ID, "Foreground Service Channel",
                NotificationManager.IMPORTANCE_DEFAULT)
            val manager = getSystemService(NotificationManager::class.java)
            manager!!.createNotificationChannel(serviceChannel)
        }
    }

}
