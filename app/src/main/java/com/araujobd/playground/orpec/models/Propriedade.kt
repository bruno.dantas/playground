package com.araujobd.playground.orpec.models

import java.util.*

data class Animal(
    private val entity: EntityDelegate,
    val name: String,
    val bornOnProperty: Boolean,
    val birthDate: Date?,
    val raca: Raca
) : Entity by entity

data class Rebanho(
    private val entity: EntityDelegate,
    val name: String,
    val especie: Especie,
    val genetica: Boolean
) : Entity by entity

data class Propriedade(
    private val entity: EntityDelegate,
    val name: String,
    val companyName: String?,
    val nirf: String?,
    val dap: String?,
    val cnpj: String?,
    val rebanhos: List<Rebanho>
) : Entity by entity {

    fun registerDocs(nirf: String?, dap: String?, cnpj: String?): Propriedade {
        return copy(nirf=nirf, dap=dap, cnpj=cnpj)
    }
}
