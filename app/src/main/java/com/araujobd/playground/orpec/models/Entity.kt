package com.araujobd.playground.orpec.models

import java.util.*

interface Entity {
    val id: String
    var active: Boolean
    val createdBy: String
    val createdAt: Date?
    var updatedBy: String?
    var updatedAt: Date?
}

data class EntityDelegate(
    override val id: String,
    override var active: Boolean,
    override val createdBy: String,
    override val createdAt: Date?,
    override var updatedBy: String?,
    override var updatedAt: Date?
) : Entity