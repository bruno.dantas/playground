package com.araujobd.playground.orpec.models


data class Raca(
    private val entity: EntityDelegate,
    val name: String
): Entity by entity

data class Especie(
    private val entity: EntityDelegate,
    val name: String,
    val racas: List<Raca>
) : Entity by entity

