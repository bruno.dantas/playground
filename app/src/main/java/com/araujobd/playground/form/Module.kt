package com.araujobd.playground.form

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val formModule = module {
    viewModel { UserViewModel() }
}