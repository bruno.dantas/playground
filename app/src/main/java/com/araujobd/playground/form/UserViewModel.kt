package com.araujobd.playground.form

import androidx.lifecycle.*

class UserViewModel : ViewModel() {

    val form = LoginForm()
    val selected = MutableLiveData<String>()

    init {
        form.options.updateEntries(listOf("1", "2", "3", "4"))
    }

    fun onClick() {
        selected.postValue(form.options.getValue())
    }
}

data class LoginForm(
    val username:StringField = StringField(::required, ::greaterThanSix),
    val domain:StringField = StringField(::required, { s -> if(s.length < 3) "Deve ter pelo menos 3 caracteres" else null }),
    val password:StringField = StringField(),
    val options: AutoCompleteField<String> = AutoCompleteField(display = {it}, isStrict = false)
) : Form(listOf(username, domain, password, options))

abstract class Form(private val fields: List<Field>) {

    val isValid = MediatorLiveData<Boolean>()

    init {
        fields.forEach { field -> isValid.addSource(field.field) { isFormValid() } }
    }

    protected fun isFormValid() {
        val valid = fields.all { it.isValid() }
        isValid.postValue(valid)
    }
}


abstract class Field(val validators: List<(String) -> String?> = emptyList()) {

    val field= MutableLiveData<String>()
    val error: LiveData<String> = Transformations.map(field, ::errorField)

    abstract fun errorField(field: String): String?

    open fun isValid(): Boolean {
        val fieldValue = field.value ?: ""
        return errorField(fieldValue) == null
    }
}

class AutoCompleteField<T>(
    vararg validator: (String) -> String?,
    val display: (T) -> String,
    val isStrict: Boolean = false
): Field(validator.toList()) {

    var options: List<T> = listOf()
    val entries = MutableLiveData<List<String>>()


    override fun errorField(field: String): String? {
        validators.forEach { validator ->
            validator(field)?.let { error -> return error }
        }
        if (isStrict) {
            return if (entries.value?.contains(field) == true) null else "Valor inválido"
        }
        return null
    }

    fun updateEntries(options: List<T>) {
        this.options = options
        setOptions()
    }

    private fun setOptions() {
        entries.postValue(this.options.map(display))
    }

    fun getValue(): String = field.value ?: ""

}

class StringField(vararg validator: (String) -> String?): Field(validator.toList()) {

    override fun errorField(field: String): String? {
        validators.forEach { validator ->
            validator(field)?.let { error -> return error }
        }
        return null
    }
}

fun required(field: String) =
    if (field.isBlank()) "Campo obrigatório" else null


fun greaterThanSix(field: String) =
    if (field.length < 6) "Campo deve ter pelo menos 6 caracteres" else null
