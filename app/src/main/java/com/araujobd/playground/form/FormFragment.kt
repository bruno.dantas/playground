package com.araujobd.playground.form

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.araujobd.playground.R
import com.araujobd.playground.databinding.FragmentFormBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class FormFragment : Fragment() {

    private val userViewModel by viewModel<UserViewModel>()
    private lateinit var binding: FragmentFormBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
/*
        return DataBindingUtil.inflate(inflater, R.layout.fragment_form, container, false)
            .run {
                binding = this
                view
            }
*/
        return FragmentFormBinding.inflate(inflater, container, false)
            .run {
                binding = this
                lifecycleOwner = this@FormFragment.viewLifecycleOwner
                viewmodel = userViewModel
                root
            }
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        userViewModel.selected.observe(viewLifecycleOwner, Observer {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        })
/*
        val adapter = ArrayAdapter<String>(requireContext(), R.layout.dropdown_menu_popup_item,
            listOf("1", "2", "3", "4"))
        binding.etOptions.setAdapter(adapter)
*/
    }
}