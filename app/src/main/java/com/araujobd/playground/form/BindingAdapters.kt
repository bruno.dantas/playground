package com.araujobd.playground.form

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.ListView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter
import androidx.databinding.InverseBindingListener
import com.araujobd.playground.R
import com.google.android.material.textfield.TextInputLayout

@BindingAdapter("error")
fun setError(inputLayout: TextInputLayout, error: String?) {
    inputLayout.error = error
}

@BindingAdapter("entries")
fun bindTextView(autoComplete: AutoCompleteTextView, entries: List<Any>?) {
    entries?.let {
        val adapter = ArrayAdapter(autoComplete.context, R.layout.dropdown_menu_popup_item, entries)
        autoComplete.setAdapter(adapter)
    }
}

@BindingAdapter("valueAttrChanged")
fun AutoCompleteTextView.setListener(listener: InverseBindingListener?) {
    this.onItemSelectedListener = if (listener != null) {
        object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                listener.onChange()
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                listener.onChange()
            }
        }
    } else {
        null
    }
}


/*
@get:InverseBindingAdapter(attribute = "value")
@set:BindingAdapter("value")
var AutoCompleteTextView.selectedValue: Any?
    get() = if (listSelection != ListView.INVALID_POSITION) adapter.getItem(listSelection) else null
    set(value) {
        val newValue = value ?: adapter.getItem(0)
        setText(newValue.toString(), true)
        if (adapter is ArrayAdapter<*>) {
            val position = (adapter as ArrayAdapter<Any?>).getPosition(newValue)
            listSelection = position
        }
    }
*/
