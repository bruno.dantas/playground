package com.araujobd.playground.customform.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import com.araujobd.playground.R
import com.araujobd.playground.databinding.FormWrapperBinding

class FormWrapper @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defStyle: Int = 0
) : ConstraintLayout(context, attributeSet, defStyle) {

    private val binding: FormWrapperBinding

    init {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        binding = FormWrapperBinding.inflate(inflater, this, true)
    }

    fun setSteps(step: Step) {
        binding.steps = step
    }

    override fun addView(child: View?, index: Int, params: ViewGroup.LayoutParams?) {
        val mainViews = listOf(R.id.toolbar, R.id.next, R.id.appbar, R.id.form_container,
            R.id.include_stepper, R.id.nested_scroll, R.id.fields_container)
        if (mainViews.contains(child?.id)) {
            super.addView(child, index, params)
        } else {
            binding.fieldsContainer.addView(child, index, params)
        }
    }
}