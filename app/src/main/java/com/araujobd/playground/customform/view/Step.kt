package com.araujobd.playground.customform.view

data class Step(val name: String, val description: String)