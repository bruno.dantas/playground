package com.araujobd.playground.customform

import com.araujobd.playground.R
import com.araujobd.playground.base.BaseFragment
import com.araujobd.playground.customform.view.Step
import com.araujobd.playground.databinding.FragmentCustomFormBinding
import kotlinx.android.synthetic.main.form_wrapper.view.*

class CustomFormFragment : BaseFragment<FragmentCustomFormBinding>(R.layout.fragment_custom_form) {

    override fun onViewCreated() {
        binding.steps = Step("Steps", "Button Next")
        binding.form.toolbar.title = "Toolbar"
    }
}