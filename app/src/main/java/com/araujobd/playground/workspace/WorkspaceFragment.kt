package com.araujobd.playground.workspace

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.araujobd.playground.databinding.FragmentWorkspaceBinding

class WorkspaceFragment : Fragment() {
    private lateinit var binding: FragmentWorkspaceBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentWorkspaceBinding.inflate(inflater, container, false)
            .run {
                lifecycleOwner = this@WorkspaceFragment.viewLifecycleOwner
                binding = this
                root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btNested.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionWorkspaceFragmentToNestedNavigation("2")
            )
        }

        binding.btForm.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionWorkspaceFragmentToFormFragment()
            )
        }

        binding.btCustomForm.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionCustomForm()
            )
        }

        binding.btRegisterForm.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionRegisterForm()
            )
        }

        binding.btCamera.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionWorkspaceFragmentToTakePhotoFragment()
            )
        }

        binding.btPickers.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionWorkspaceFragmentToDateFragment()
            )
        }
        binding.btCardSwipe.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionWorkspaceFragmentToCardsSwipeFragment()
            )
        }

        binding.btLocation.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionWorkspaceFragmentToLocationFragment()
            )
        }
        binding.btList.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionWorkspaceFragmentToListFragment()
            )
        }

        binding.btSide.setOnClickListener {
            findNavController().navigate(
                WorkspaceFragmentDirections.actionSideFragment()
            )
        }
    }
}