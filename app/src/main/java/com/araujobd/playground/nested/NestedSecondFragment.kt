package com.araujobd.playground.nested

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.araujobd.playground.R
import com.araujobd.playground.databinding.FragmentNestedSecondBinding

class NestedSecondFragment : Fragment() {

    private val args by navArgs<NestedSecondFragmentArgs>()
    private lateinit var binding: FragmentNestedSecondBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentNestedSecondBinding.inflate(inflater, container, false)
            .run {
                lifecycleOwner = this@NestedSecondFragment.viewLifecycleOwner
                binding = this
                root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvArgs.text = args.id

        binding.btBack.setOnClickListener {
            findNavController().popBackStack(R.id.workspaceFragment, false)
        }
    }

}