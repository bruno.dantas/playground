package com.araujobd.playground.nested

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.araujobd.playground.databinding.FragmentNestedFirstBinding

class NestedFirstFragment : Fragment() {

    private val args by navArgs<NestedFirstFragmentArgs>()
    private lateinit var binding: FragmentNestedFirstBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return FragmentNestedFirstBinding.inflate(inflater, container, false)
            .run {
                lifecycleOwner = this@NestedFirstFragment.viewLifecycleOwner
                binding = this
                root
            }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvArgs.text = args.id
        binding.btNext.setOnClickListener {
            findNavController().navigate(
                NestedFirstFragmentDirections
                    .actionNestedFirstFragmentToNestedSecondFragment(args.id)
            )
        }
    }
}