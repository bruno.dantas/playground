object Config {
    const val applicationId = "com.araujobd.playground"
    const val compileSdkVersion = 29
    const val targetSdkVersion = 29
    const val minSdkVersion = 22
    const val versionName = "1.0"
    const val versionCode = 1

}
