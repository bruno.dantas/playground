object Libs {


    /*  Plugins  */
    const val buildGradlePlugin: String = "com.android.tools.build:gradle:" + Versions.buildGradle

    const val kotlinGradlePlugin: String = "org.jetbrains.kotlin:kotlin-gradle-plugin:" +
            Versions.kotlin

    const val realmGraglePlugin: String = "io.realm:realm-gradle-plugin:" + Versions.realm

    const val navigationSafeArgsPlugin: String =
        "androidx.navigation:navigation-safe-args-gradle-plugin:" + Versions.navigation

    /*  Test  */
    const val espressoCore: String =
        "androidx.test.espresso:espresso-core:" + Versions.espressoCore

    const val mockito = "org.mockito:mockito-core:" + Versions.mockito

    const val androidxTestRunner: String = "androidx.test:runner:" + Versions.androidxTestRunner

    const val junit: String = "junit:junit:" + Versions.junit

    /*  Project */
    const val appCompat: String = "androidx.appcompat:appcompat:" + Versions.appCompat

    const val constraintLayout: String = "androidx.constraintlayout:constraintlayout:" +
            Versions.constraintLayout

    const val coreKtx: String = "androidx.core:core-ktx:" + Versions.coreKtx

    const val koin: String = "org.koin:koin-androidx-viewmodel:" + Versions.koin

    const val rxJava: String = "io.reactivex.rxjava2:rxjava:" + Versions.rxJava

    const val rxKotlin: String = "io.reactivex.rxjava2:rxkotlin:" + Versions.rxKotlin

    const val rxAndroid: String = "io.reactivex.rxjava2:rxandroid:" + Versions.rxAndroid

    const val rxBinding: String = "com.jakewharton.rxbinding2:rxbinding:" + Versions.rxBinding

    const val realmExtensions: String = "com.github.vicpinm:krealmextensions:" +
            Versions.realmExtensions

    const val preference = "androidx.preference:preference:" + Versions.preference

    const val cardView = "androidx.cardview:cardview:" + Versions.cardView

    const val material = "com.google.android.material:material:" + Versions.material

    const val lifecycleExtensions = "androidx.lifecycle:lifecycle-extensions:" + Versions.lifecycle

    const val lifecycleKapt = "androidx.lifecycle:lifecycle-compiler:" + Versions.lifecycle

    const val workRuntime = "androidx.work:work-runtime-ktx:" + Versions.workManager

    const val workRx = "androidx.work:work-rxjava2:" + Versions.workManager

    const val okhttp = "com.squareup.okhttp3:logging-interceptor:" + Versions.okhttp

    const val retrofit = "com.squareup.retrofit2:retrofit:" + Versions.retrofit

    const val retrofitAdapter = "com.squareup.retrofit2:adapter-rxjava2:" + Versions.retrofit

    const val retrofitConverter = "com.squareup.retrofit2:converter-gson:" + Versions.retrofit

    const val navigationFragment = "androidx.navigation:navigation-fragment-ktx:" +
            Versions.navigation

    const val navigationUi = "androidx.navigation:navigation-ui-ktx:" + Versions.navigation

    const val googleLocation =
        "com.google.android.gms:play-services-location:" + Versions.googleLocation

    const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:" + Versions.swipeRefreshLayout

    const val cameraxCore =  "androidx.camera:camera-core:" + Versions.cameraxVersion

   const val cameraxCamera2 = "androidx.camera:camera-camera2:" + Versions.cameraxVersion

    const val rabbitmq = "com.meltwater:rxrabbit:" + Versions.rabbitmqVersion

    const val rabbitmqq = "com.rabbitmq:amqp-client:"

    const val viewPager2 = "androidx.viewpager2:viewpager2:" + Versions.viewPager2

    const val circleIndicator = "me.relex:circleindicator:" + Versions.circleIndicator

    const val rxLocation = "com.patloew.rxlocation:rxlocation:" + Versions.rxLocation
}
