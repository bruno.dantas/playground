import org.gradle.plugin.use.PluginDependenciesSpec
import org.gradle.plugin.use.PluginDependencySpec

object Versions {

    const val kotlin: String = "1.3.50"

    const val buildGradle: String = "3.5.0"

    const val buildSrcVersions: String = "0.4.2"

    const val junit: String = "4.12"

    const val espressoCore: String = "3.2.0"

    const val androidxTestRunner: String = "1.2.0"

    const val mockito = "2.23.0"

    const val appCompat: String = "1.0.2"

    const val constraintLayout: String = "1.1.3"

    const val coreKtx: String = "1.0.2"

    const val koin: String = "2.0.1"

    const val rxJava: String = "2.2.12"

    const val rxKotlin: String = "2.4.0"

    const val rxAndroid: String = "2.1.1"

    const val rxBinding: String = "2.1.1"

    const val realm: String = "5.14.0"

    const val realmExtensions: String = "2.5.0"

    const val preference = "1.1.0-alpha04"

    const val cardView = "1.0.0"

    const val material = "1.1.0-beta01"

    const val lifecycle = "2.0.0"

    const val okhttp = "3.11.0"

    const val retrofit = "2.4.0"

    const val navigation = "2.1.0-alpha04"

    const val workManager = "2.0.1"

    const val googleLocation = "17.0.0"

    const val swipeRefreshLayout = "1.1.0-alpha02"

    const val cameraxVersion = "1.0.0-alpha04"

    const val rabbitmqVersion = "1.3.0"

    const val viewPager2 = "1.0.0"

    const val circleIndicator = "2.1.4"

    const val rxLocation = "1.0.5"
}

val PluginDependenciesSpec.buildSrcVersions: PluginDependencySpec
    inline get() =
        id("de.fayard.buildSrcVersions").version(Versions.buildSrcVersions)
